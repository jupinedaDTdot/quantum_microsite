import React from "react";
import logoSmall from "../../assets/Deloitte_Quantum_logo_white.svg";
import Helmet from "react-helmet";
import "../../node_modules/normalize.css";
import "../styles/main.scss";
import icon from "../../static/favicon.ico";

export default () => (
  <div>
    <Helmet
      title="Deloitte Quantum"
      meta={[
        {
          name: "description",
          content: "Connecting you with the Deloitte Quantum team."
        },
        { name: "keywords", content: "Deloitte, Quantum, Computing" }
      ]}
      link={[
        { rel: "icon", type: "image/png", href: `${icon}`, sizes: "16x16" }
      ]}
    />
    <main className="main">
      <div className="wrapper">
        <a href="https://www2.deloitte.com/ca/en.html">
          <img className="logo" src={logoSmall} alt="Deloitte Quantum Logo" />
        </a>
        <h1 className="main_heading">
          The <span className="heading_span">nature of computing </span>
          <br />
          is <strong>changing</strong>.
        </h1>
        <p className="main_para">
          Quantum computing will help develop the innovations of tomorrow and
          we're drawing on Deloitte's renowned industry expertise today to
          leverage the capabilities of Quantum technologies as only Deloitte
          can.
        </p>
        <p className="main_para">
          Get in touch with us to learn more about what Quantum technology can
          do for your organization.
        </p>
        <a className="main_button" href="mailto:cvanherpt@deloitte.ca">
          Learn More
        </a>
      </div>
    </main>
  </div>
);
