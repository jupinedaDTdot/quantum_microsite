import React from "react";

const NotFoundPage = () => (
  <div>
    <h1> NOT FOUND </h1>{" "}
    <p>
      {" "}
      You just hit a route that doesn't exist. Click on the back button to go
      back to the previous page.
    </p>
  </div>
);

export default NotFoundPage;
